from django.urls import path
from recipes.views import recipe_list, show_recipe, create_recipe, edit_recipe

urlpatterns = [
    path("recipes/", recipe_list, name="recipe_list"),
    path("recipes/<int:id>/", show_recipe, name="show_recipe"),
    path("recipes/create/", create_recipe, name="create_recipe"),
    path("recipes/edit/<int:id>/", edit_recipe, name="edit_recipe"),
]

# added , name="show_recipe" to end of path

#changed "recipes/" in path function
# of url patterns in app called recipes
# in urls file to "recipes/<int:id>/"

# now after this go to views.py
# and add the id parameter

# now that we added a new view function to
# views.py getting all of the data
# using the all method
# and created a dictionary called context with
# all the recipes in it
# we render the html template
# ex. return render(request, "recipes/list.html, context")
# after we finish the views function we must
# configure our URL path(s)
# in this file
# next to import show_recipe we added recipe_list
# we also added a new path above show_recipe path
# path("recipes/", recipe_list),

# the urls in urls.py in application
# are directly correlated to the function parameters
# in the views.py function(s)

# tip** urls doesn't care where you put placeholder parameter

# id is a placeholder
