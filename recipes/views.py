from django.shortcuts import render, get_object_or_404, redirect
from recipes.models import Recipe
from recipes.forms import RecipeForm

# Create your views here.
def show_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    context = {
        "recipe_object": recipe,
    }
    return render(request, "recipes/detail.html", context)

def recipe_list(request):
    recipes = Recipe.objects.all()
    context = {
        "recipe_list": recipes,
    }
    return render(request, "recipes/list.html", context)


def create_recipe(request):
    if request.method == "POST":
        # we should use the form to validate
        # the values and save them to
        # the database
        form = RecipeForm(request.POST)
        if form.is_valid():
            form.save()
            # if all goes well, we can redirect
            # the browser to another page and
            # leave the function
            return redirect("recipe_list")
    else:
        form = RecipeForm()
            # Create an instance of the Django
            # model form class
    context = {
        "form": form,
        }
    return render(request, "recipes/create.html", context)


def edit_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    if request.method == "POST":
        form = RecipeForm(request.POST, instance=recipe)
        if form.is_valid():
            form.save()
            return redirect("show_recipe", id=id)
    else:
        form = RecipeForm(instance=recipe)
    context = {
        "recipe_object": recipe,
        "recipe_form": form,
    }
    return render(request, "recipes/edit.html", context)








# to add an edit recipe page

# WE already had a recipe form created
# BUT in the instance we dont,
# WE MUST CREATE ONE

# created new html within recipes directory
# edited form variable to match context of
# edit_recipe function

# added url path to urls.py in recipes
# application directory

# created edit_recipe function
# very similar to create_recipe function







        # put the form in the context
        # Render the HTML template with the form

# fixed value error on webpage
# was because of else: statement
# was inside if statement rather than on its
# own block


# defining a new view function
    # def recipe_list(request):
    # but this time were getting all
    # of the Recipe objects
    # we dont need 1 id so we just put in
    # request as the parameter
    # underneath we create a variable
    # within the function  and
    # that  variable uses the all method

    # recipes = Recipe.objects.all()

# TypeError at /recipes/2/

# show_recipe() got an unexpected
# keyword argument 'id'
# What does it mean?
# That's a Python error, so it means
# that Django is calling our function
# with an extra parameter named id!
# To fix this error, we have to add
# that parameter to our function.
# added id as a parameter to show
# recipe function formerly was
# def show_recipe(request):
# now is def show_recipe(request, id):
# Now that you're getting the id from
# the URL path, you can use it to get
# the data from the database. We use the
# get_object_or_404 shortcut method so
# that Django will create a 404 error when
# the recipe doesn't exist for the provided id.
