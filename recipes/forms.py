from django import forms
from recipes.models import Recipe


class RecipeForm(forms.ModelForm):
    email_address = forms.EmailField(max_length=300)

    class Meta:
        model = Recipe
        fields = [
            "title",
            "picture",
            "description",
        ]


# revised version above

# from django.forms import ModelForm
# from recipes.models import Recipe

# class RecipeForm(ModelForm):    # Step 1
#     class Meta:                 # Step 2
#         model = Recipe          # Step 3
#         fields = [              # Step 4
#             "title",            # Step 4
#             "picture",          # Step 4
#             "description",      # Step 4
#         ]                       # Step 4
